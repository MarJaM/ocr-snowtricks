import Input from './Input';

export default class InputList {
  constructor(listContainerId, templateId) {
    /**
     * @type {Element}
     */
    this.listContainer = document.querySelector(`#${listContainerId}`);

    /**
     * @type {string}
     */
    this.formPrototype = this.listContainer.getAttribute('data-prototype');

    /**
     * @type {string}
     */
    this.counter = this.listContainer.getAttribute('data-widget-counter');

    /**
     * @type {Element}
     */
    this.templateElement = document.querySelector(`#${templateId}`);

    this.imageIdTemplate = 'trick-video-thumbnail-__name__';

    this.inputs = [];

    this.init();
  }

  init() {
    this.listContainer.querySelectorAll('.template-content').forEach((content) => {
      const imageEl = content.querySelector('img');
      const inputEl = content.querySelector('input[type=file], input[type=text]');
      const hiddenEL = content.querySelector('input[type=hidden]');
      const input = this.initInput(inputEl.id, imageEl);
      if (hiddenEL.value.length > 0) {
        inputEl.dispatchEvent(new Event('input'));
      } else {
        input.removeMe();
      }
    });
  }

  addNewInput() {
    const template = document.importNode(this.templateElement.content, true);
    const imageId = this.imageIdTemplate.replace('__name__', this.counter.toString()).trim();
    template.querySelector('img.trick-details-thumbnail').id = imageId;

    const formPrototype = this.formPrototype.replace(/__name__/g, this.counter.toString()).trim();
    const inputElement = document.createRange().createContextualFragment(formPrototype).querySelector('input[type=file], input[type=text]');

    template.querySelector('.input-container').append(inputElement);
    this.listContainer.appendChild(template);

    this.initInput(this.getLastInputElement().id, imageId);
    this.counter++;
  }

  initInput(inputId, imageId) {
    const input = new Input(inputId, imageId, this);
    this.inputs.push(input);
    return input;
  }

  getLastInputElement() {
    const inputElements = this.listContainer.querySelectorAll('input[type=file], input[type=text]');
    return inputElements[inputElements.length - 1];
  }
}
