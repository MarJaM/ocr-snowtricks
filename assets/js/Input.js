import Image from './Image';

export default class Input {
  constructor(inputId, imageId, inputList = null) {
    /**
     * @type {Element}
     */
    this.inputElement = document.querySelector(`#${inputId}`);

    /**
     * @type {Image}
     */
    this.mainImage = new Image(imageId);

    /**
     * @type {Element}
     */
    this.inputContainerElement = this.getParentElement('field-container');

    /**
     * @type {null|InputList|FileInputList}
     */
    this.inputList = inputList;

    if (this.inputList) {
      this.fieldContainer = this.getParentElement('template-content');
      this.modalActivatorElement = this.fieldContainer.querySelector('button.modal-activator');
    }

    if (this.modalActivatorElement) {
      this.modalElement = document.querySelector('.modal');
      this.modalFormElement = this.modalElement.querySelector('form');
      this.modalCloseElement = document.querySelector('.modal-close');
      this.modalBackgroundElement = document.querySelector('.modal-background');
      this.modalSubmitElement = document.querySelector('#modal-submit');
      this.modalControlElement = document.querySelector('#modal-input-control');
      this.modalInputElement = document.querySelector('#new_video_url');
      this.addModalActivatorEvent();
    }

    this.addCloneMeEvent();
    this.addChangeIconEventOnInput();
    this.addTrashButtonEvent();
  }

  getParentElement(className) {
    let containerEl = this.inputElement;
    while (!containerEl.classList.contains(className)) {
      containerEl = containerEl.parentElement;
    }
    return containerEl;
  }

  getTrashButtonElement() {
    return this.inputContainerElement.querySelector('button.trash');
  }

  async handleNewVideoSubmission() {
    this.modalControlElement.classList.add('is-loading');
    const body = new FormData(this.modalFormElement);
    const response = await fetch('/tricks/getVideoThumbnail', {
      method: 'POST',
      body,
    });
    this.modalControlElement.classList.remove('is-loading');
    if (!response.ok) return Promise.reject();

    return response.text().then((url) => url);
  }

  addModalHandlingEvents() {
    const toggleModal = () => {
      this.modalElement.classList.remove('is-active');
      this.modalBackgroundElement.removeEventListener('click', toggleModal);
      this.modalCloseElement.removeEventListener('click', toggleModal);
      // eslint-disable-next-line no-use-before-define
      this.modalSubmitElement.removeEventListener('click', handleFormSubmission);
    };

    const handleFormSubmission = (event) => {
      event.preventDefault();
      this.handleNewVideoSubmission()
        .then((thumbnailUrl) => {
          const iframe = this.fieldContainer.querySelector('iframe');
          if (iframe) iframe.remove();
          this.mainImage.display(thumbnailUrl);
          this.inputElement.setAttribute('value', this.modalInputElement.value);
          this.inputElement.dispatchEvent(new Event('input'));
          toggleModal();
        });
    };

    this.modalBackgroundElement.addEventListener('click', toggleModal);
    this.modalCloseElement.addEventListener('click', toggleModal);
    this.modalSubmitElement.addEventListener('click', handleFormSubmission);
  }

  addModalActivatorEvent() {
    const toggleModal = () => {
      this.modalElement.classList.add('is-active');
      this.addModalHandlingEvents();
    };

    this.modalActivatorElement.addEventListener('click', toggleModal);
  }

  addCloneMeEvent() {
    if (this.inputList) {
      this.inputElement.addEventListener('input', () => {
        if (this.inputList.getLastInputElement().value.length > 0) this.inputList.addNewInput();
      }, { once: true });
    }
  }

  addChangeIconEventOnInput() {
    this.inputElement.addEventListener('input', () => {
      this.changeIcon();
    }, { once: true });
  }

  removeMe() {
    this.fieldContainer.remove();
    delete this;
  }

  addTrashButtonEvent() {
    this.getTrashButtonElement().addEventListener('click', () => {
      this.inputElement.value = '';
      const hiddenEl = this.inputContainerElement.querySelector('input[type=hidden]');
      if (hiddenEl) hiddenEl.value = '';
      this.mainImage.reset();
      this.changeIcon();
      this.addChangeIconEventOnInput();
      if (this.inputList) this.removeMe();
    });
  }

  changeIcon() {
    this.modalActivatorElement.classList.toggle('is-primary');
    this.modalActivatorElement.classList.toggle('is-small');
    this.modalActivatorElement.classList.toggle('is-marginless');

    const iconsContainer = this.inputContainerElement.querySelector('.icon-container');
    iconsContainer.classList.toggle('is-right');
    iconsContainer.classList.toggle('is-centered');
    iconsContainer.querySelector('.file-icon.add').classList.toggle('is-hidden');
    iconsContainer.querySelector('.file-icon.change').classList.toggle('is-hidden');

    this.inputContainerElement.querySelector('.input-text').classList.toggle('is-hidden');

    this.getTrashButtonElement().classList.toggle('is-hidden');
  }
}
