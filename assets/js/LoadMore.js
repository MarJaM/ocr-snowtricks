export default class LoadMore {
  constructor(resourceName) {
    this.page = 1;
    this.resourceName = resourceName;
    this.listContainer = document.getElementById(`${resourceName}-list-container`);
    this.itemContainerEl = this.listContainer.querySelector(`.${resourceName}-container`);
    this.button = document.getElementById(`${resourceName}-load-more-button`) || null;
    this.button.addEventListener('click', () => this.loadMore());
  }

  loadMore() {
    this.page++;
    const searchParams = new URLSearchParams();
    searchParams.append('page', this.page);
    const url = `${this.button.getAttribute('data-link')}?${searchParams.toString()}`;
    this.button.classList.add('is-loading');
    fetch(url)
    // eslint-disable-next-line consistent-return
      .then((response) => {
        this.button.classList.remove('is-loading');
        if (response.ok) {
          return response.json();
        }
      })
      .then(({ items }) => {
        items.forEach((item) => this.display(item));
        if (items.length < 10) {
          this.button.setAttribute('disabled', 'true');
          this.button.textContent = `No more ${this.resourceName} available`;
        }
        this.afterFetch();
      })
      .catch(() => this.button.classList.remove('is-loading'));
  }

  // eslint-disable-next-line class-methods-use-this
  afterFetch() {}

  // eslint-disable-next-line no-unused-vars,class-methods-use-this
  display(item) {}
}
