const toggleButton = document.getElementById("button-toggle-media");
const mediasContainer = document.getElementById("medias-container");

const toggleMedias = () => {
    mediasContainer.classList.toggle('is-hidden-mobile');
    toggleButton.firstElementChild.textContent = mediasContainer.classList.contains('is-hidden-mobile') ? 'Show medias' : 'Hide medias';
}

document.addEventListener('DOMContentLoaded', () => {
    toggleButton.addEventListener('click', toggleMedias);
});

