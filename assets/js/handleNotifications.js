document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach((deleteButton) => {
    const notificationEl = deleteButton.parentNode;

    deleteButton.addEventListener('click', () => {
      notificationEl.parentNode.removeChild(notificationEl);
    });
  });
});
