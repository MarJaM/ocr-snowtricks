import LoadMore from './LoadMore';

class CommentList extends LoadMore {
  constructor() {
    super('comment');
  }

  display(comment) {
    const newCommentContainer = this.itemContainerEl.cloneNode(true);
    newCommentContainer.querySelector('.comment-author').textContent = comment.user.pseudo;
    newCommentContainer.querySelector('.comment-message').textContent = comment.message;
    this.listContainer.appendChild(newCommentContainer);
  }
}

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new CommentList();
});
