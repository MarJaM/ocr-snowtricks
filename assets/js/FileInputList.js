import FileInput from './FileInput';
import InputList from './InputList';

export default class FileInputList extends InputList {
  constructor(listContainerId, templateId) {
    super(listContainerId, templateId);
    this.imageIdTemplate = 'trick-list-image-__name__';
  }

  initInput(inputId, imageId) {
    const input = new FileInput(inputId, imageId, this);
    this.inputs.push(input);
    return input;
  }
}
