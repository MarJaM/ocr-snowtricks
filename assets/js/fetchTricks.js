import LoadMore from './LoadMore';
import DeleteModal from './DeleteModal';

class TrickList extends LoadMore {
  constructor() {
    super('trick');
    this.deleteModal = new DeleteModal();
    this.deleteModal.initEvents();
  }

  display(trick) {
    const newTrickContainer = this.itemContainerEl.cloneNode(true);

    newTrickContainer.querySelector('figure a').setAttribute('href', `/tricks/details/${trick.slug}`);
    newTrickContainer.querySelector('figure img').setAttribute('src', this.getImagePath(trick));
    newTrickContainer.querySelector('figure img').setAttribute('alt', `Image principale de ${trick.title}`);

    newTrickContainer.querySelector('.card-content a').setAttribute('href', `/tricks/details/${trick.slug}`);
    newTrickContainer.querySelector('.title').textContent = trick.title;

    const controlButtons = newTrickContainer.querySelector('.control-buttons');
    if (this.button.getAttribute('data-is-admin')
        || (this.button.getAttribute('data-user') === trick.author.id.toString())
    ) {
      controlButtons.classList.remove('is-hidden');
      controlButtons.querySelector('button.delete-trick').setAttribute('data-link', `/tricks/${trick.slug}`);
    } else {
      controlButtons.classList.add('is-hidden');
    }

    this.listContainer.appendChild(newTrickContainer);
    this.displayArrow();
  }

  displayArrow() {
    const list = this.listContainer.querySelectorAll(`.${this.resourceName}-container`);

    if (list.length >= 15) {
      document.getElementById('arrow-up').classList.remove('is-hidden');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  getImagePath(trick) {
    if (trick.mainImage) {
      return `/uploads/images/${trick.mainImage.fileName}`;
    } if (trick.images && trick.images.length > 0) {
      return `/uploads/images/${trick.images[0].fileName}`;
    }

    return '/images/home_background.jpg';
  }

  afterFetch() {
    this.deleteModal.resetDeleteButtonsEvents();
  }
}

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new TrickList();
});
