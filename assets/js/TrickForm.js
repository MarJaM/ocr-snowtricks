import FileInputList from './FileInputList';
import FileInput from './FileInput';
import InputList from './InputList';

export default class TrickForm {
  constructor(formId) {
    /**
     * @type {Element}
     */
    this.form = document.querySelector(`#${formId}`);
  }

  init() {
    this.mainImageInput = new FileInput('new_trick_mainImage_imageInput', 'trick-main-image');
    if (this.mainImageInput.inputElement.value.length > 0 || this.mainImageInput.mainImage.isDisplayed()) this.mainImageInput.inputElement.dispatchEvent(new Event('input'));

    this.imageList = new FileInputList('trick-details-images-container', 'trick-details-images-template');
    this.imageList.addNewInput();

    this.urlList = new InputList('trick-details-videos-container', 'trick-details-videos-template');
    if (!this.urlList.getLastInputElement()) this.urlList.addNewInput();
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const form = new TrickForm('trick-form');
  form.init();
});
