import DeleteModal from './DeleteModal';

document.addEventListener('DOMContentLoaded', () => {
  const deleteModal = new DeleteModal();
  deleteModal.initEvents();
});
