import Input from './Input';

export default class FileInput extends Input {
  constructor(inputId, imageId, inputList = null) {
    super(inputId, imageId, inputList);
    this.addImageDisplayEvent();
  }

  addImageDisplayEvent() {
    this.inputElement.addEventListener('input', () => {
      this.mainImage.display(this.inputElement.files[0]);
    });
  }

  changeIcon() {
    const toToggle = this.inputContainerElement.classList.contains('boxed') ? 'is-boxed' : 'is-right';
    this.inputContainerElement.classList.toggle(toToggle);
    this.inputContainerElement.classList.toggle('is-right');
    this.inputContainerElement.classList.toggle('is-small');
    this.inputContainerElement.classList.toggle('is-primary');

    const iconsContainer = this.inputContainerElement.querySelector('.icon-container');
    iconsContainer.querySelector('.file-icon.add').classList.toggle('is-hidden');
    iconsContainer.querySelector('.file-icon.change').classList.toggle('is-hidden');

    this.inputContainerElement.querySelector('.input-text').classList.toggle('is-hidden');

    this.getTrashButtonElement().classList.toggle('is-hidden');
  }
}
