export default class DeleteModal {
  constructor() {
    this.deleteButtons = document.querySelectorAll(('.button.delete-trick'));
    this.modal = document.querySelector('#delete-modal');
    this.modalBackground = this.modal.querySelector(('.modal-background'));
    this.modalClose = this.modal.querySelector(('.modal-close'));
    this.modalCancel = this.modal.querySelector(('#cancel-button'));
    this.modalDelete = this.modal.querySelector(('#delete-button'));
  }

  openModal(event) {
    this.modalDelete.setAttribute('data-link', event.currentTarget.getAttribute('data-link'));
    this.modalDelete.setAttribute('data-token', event.currentTarget.getAttribute('data-token'));
    this.modal.classList.add('is-active');
  }

  closeModal() {
    this.modal.classList.remove('is-active');
  }

  deleteTrick() {
    const body = new URLSearchParams();
    body.append('_token', this.modalDelete.getAttribute('data-token'));
    fetch(this.modalDelete.getAttribute('data-link'), {
      method: 'DELETE',
      body,
    }).then((response) => { if (response.ok) window.location = '/tricks'; });
  }

  initEvents() {
    this.deleteButtons.forEach((button) => button.addEventListener('click', this.openModal.bind(this)));
    this.modalClose.addEventListener('click', () => this.closeModal());
    this.modalBackground.addEventListener('click', () => this.closeModal());
    this.modalCancel.addEventListener('click', () => this.closeModal());
    this.modalDelete.addEventListener('click', () => this.deleteTrick());
  }

  resetDeleteButtonsEvents() {
    this.deleteButtons.forEach((button) => button.removeEventListener('click', this.openModal.bind(this)));
    this.deleteButtons = document.querySelectorAll(('.button.delete-trick'));
    this.deleteButtons.forEach((button) => button.addEventListener('click', this.openModal.bind(this)));
  }
}
