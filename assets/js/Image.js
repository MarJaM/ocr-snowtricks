export default class Image {
  constructor(image) {
    /**
     * @type {Element}
     */
    this.imageEl = typeof image === 'string'
      ? document.querySelector(`#${image}`)
      : image;
  }

  /**
   * @param {File|string} image
   */
  display(image) {
    if (typeof image === 'object') {
      const reader = new FileReader();
      reader.onload = () => {
        this.imageEl.src = reader.result;
      };
      reader.readAsDataURL(image);
    } else if (image && image.length > 0) {
      this.imageEl.src = image;
    }
    this.imageEl.classList.remove('is-hidden');
  }

  reset() {
    this.imageEl.src = '';
    this.imageEl.classList.add('is-hidden');
  }

  isDisplayed() {
    return this.imageEl.getAttribute('src').length > 0;
  }
}
