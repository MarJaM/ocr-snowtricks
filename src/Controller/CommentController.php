<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CommentType;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/{trickSlug}/new", name="comment_new", methods={"POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Request $request
     * @param Trick $trick
     * @return Response
     * @ParamConverter("trick", options={"mapping": {"trickSlug": "slug"}})
     */
    public function new(Request $request, Trick $trick): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUser($this->getUser());
            $comment->setTrick($trick);
            $commentManager = $this->getDoctrine()->getManager();
            $commentManager->persist($comment);
            $commentManager->flush();
        }

        return $this->redirectToRoute('trick_show', ['slug' => $trick->getSlug()]);
    }

    /**
     * @Route("/{trickSlug}", name="comment_fetch_page", methods={"GET"})
     * @param Request $request
     * @param Trick $trick
     * @ParamConverter("trick", options={"mapping": {"trickSlug": "slug"}})
     * @return JsonResponse
     */
    public function fetchPaginatedComments(Request $request, Trick $trick) : JsonResponse
    {
        $page = $request->query->get('page');
        if ($page === null || $page < 1) {
            throw new BadRequestHttpException();
        }
        $commentRepository = $this->getDoctrine()->getRepository(Comment::class);
        $results = $commentRepository->findBy(['trick' => $trick], ['id' => 'DESC'], 10, 10 * ($page - 1));

        return $this->json(["items" => $results], 200, [], ['groups' => 'comment:get']);
    }
}
