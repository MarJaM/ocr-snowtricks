<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Form\CommentType;
use App\Form\NewTrickType;
use App\Form\NewVideoType;
use App\Repository\TrickRepository;
use App\Service\TrickFormHandler;
use Embed\Embed;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tricks")
 */
class TrickController extends AbstractController
{
    /**
     * @Route("/", name="trick_index", methods={"GET"})
     * @param TrickRepository $trickRepository
     * @param Request $request
     * @return Response
     */
    public function index(TrickRepository $trickRepository, Request $request): Response
    {
        $page = $request->query->get('page');

        if ($page === null) {
            return $this->render('trick/index.html.twig', [
                'tricks' => $trickRepository->findPage(1),
            ]);
        }

        if ($page < 1) {
            throw new BadRequestHttpException();
        }

        $results = $trickRepository->findPage((int) $page);

        return $this->json(["items" => $results], 200, [], ['groups' => ['tricks:list']]);
    }

    /**
     * @Route("/new", name="trick_new", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function new(Request $request, TrickFormHandler $trickFormHandler): Response
    {
        $trick = new Trick();
        $form = $this->createForm(NewTrickType::class, $trick);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $trick->setAuthor($this->getUser());
            $trickFormHandler->handleFormSubmission($form, $trick);
            $this->addFlash('success', 'Your trick has correctly been created.');
            return $this->redirectToRoute('trick_index');
        }

        $modalForm = $this->createForm(NewVideoType::class, null, [
            'action' => $this->generateUrl('get_video_thumbnail'),
            'method' => 'GET',
        ]);

        return $this->render('trick/new.html.twig', [
            'form' => $form->createView(),
            'modalForm' => $modalForm->createView(),
        ]);
    }

    /**
     * @Route("/details/{slug}", name="trick_show", methods={"GET"})
     * @param Trick $trick
     * @return Response
     */
    public function show(Trick $trick): Response
    {
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment, [
            'action' => $this->generateUrl('comment_new', ['trickSlug' => $trick->getSlug()]),
        ]);

        $comments = $this->getDoctrine()->getRepository(Comment::class)->findBy(['trick' => $trick], ['id' => 'DESC'], 10);

        return $this->render('trick/show.html.twig', [
            'trick' => $trick,
            'commentForm' => $commentForm->createView(),
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/{slug}/edit", name="trick_edit", methods={"GET", "PUT"})
     * @param Request $request
     * @param Trick $trick
     * @return Response
     * @IsGranted("IS_TRICK_OWNER", subject="trick")
     */
    public function edit(Request $request, Trick $trick, TrickFormHandler $trickFormHandler): Response
    {
        $form = $this->createForm(NewTrickType::class, $trick, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $trickFormHandler->handleFormSubmission($form, $trick);
            return $this->redirectToRoute('trick_show', ['slug' => $trick->getSlug()]);
        }

        $modalForm = $this->createForm(NewVideoType::class, null, [
            'action' => $this->generateUrl('get_video_thumbnail'),
            'method' => 'GET',
        ]);

        return $this->render('trick/edit.html.twig', [
            'trick' => $trick,
            'form' => $form->createView(),
            'modalForm' => $modalForm->createView(),
        ]);
    }

    /**
     * @Route("/{slug}", name="trick_delete", methods={"DELETE"})
     * @param Request $request
     * @param Trick $trick
     * @return Response
     * @IsGranted("IS_TRICK_OWNER", subject="trick")
     */
    public function delete(Request $request, Trick $trick): Response
    {
        if ($this->isCsrfTokenValid('deleteTrick', $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($trick);
            $entityManager->flush();
            return new Response(null, 204);
        }

        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/getVideoThumbnail", name="get_video_thumbnail", methods={"POST"})
     * @param Request $request
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @return Response|NotFoundHttpException
     */
    public function getVideoThumbnail(Request $request)
    {
        $form = $this->createForm(NewVideoType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $videoUrl = $form['url']->getData();
            $embed = new Embed();
            $info = $embed->get($videoUrl);
            return new Response($info->image);
        }

        return new NotFoundHttpException();
    }
}
