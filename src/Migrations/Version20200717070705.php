<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200717070705 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media ADD trick_owner_id INT DEFAULT NULL, ADD embed_code VARCHAR(510) DEFAULT NULL, CHANGE trick_id trick_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C1BEFE546 FOREIGN KEY (trick_owner_id) REFERENCES trick (id)');
        $this->addSql('CREATE INDEX IDX_6A2CA10C1BEFE546 ON media (trick_owner_id)');
        $this->addSql('ALTER TABLE trick DROP FOREIGN KEY FK_D8F0A91EE4873418');
        $this->addSql('DROP INDEX UNIQ_D8F0A91EE4873418 ON trick');
        $this->addSql('ALTER TABLE trick DROP main_image_id, CHANGE author_id author_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10C1BEFE546');
        $this->addSql('DROP INDEX IDX_6A2CA10C1BEFE546 ON media');
        $this->addSql('ALTER TABLE media DROP trick_owner_id, DROP embed_code, CHANGE trick_id trick_id INT NOT NULL');
        $this->addSql('ALTER TABLE trick ADD main_image_id INT DEFAULT NULL, CHANGE author_id author_id INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE trick ADD CONSTRAINT FK_D8F0A91EE4873418 FOREIGN KEY (main_image_id) REFERENCES media (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D8F0A91EE4873418 ON trick (main_image_id)');
    }
}
