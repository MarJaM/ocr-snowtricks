<?php


namespace App\Service;


use App\Entity\Trick;
use App\Entity\TrickMainImage;
use Doctrine\ORM\EntityManagerInterface;
use Embed\Embed;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class TrickFormHandler
{
    private $slugger;
    private $uploader;
    private $entityManager;
    private $trick;

    public function __construct(SluggerInterface $slugger, FileUploader $uploader, EntityManagerInterface $entityManager)
    {
        $this->slugger = $slugger;
        $this->uploader = $uploader;
        $this->entityManager = $entityManager;
    }

    private function handleMainImageSubmission($mainImage): ?TrickMainImage
    {
        $imageFile = $mainImage['imageInput']->getData();
        $sentFileName = $mainImage['fileName']->getData();
        $image = $this->trick->getMainImage();

        if (!empty($imageFile)) {
            $image = $image ?? new TrickMainImage();
            $image->setFileName($this->uploader->upload($mainImage['imageInput']->getData()));
        } elseif (empty($sentFileName)) {
            if ($image instanceof TrickMainImage) {
                $this->entityManager->remove($image);
                $this->entityManager->flush();
            }
            $image = null;
        }
        return $image;
    }

    private function handleImagesSubmission(FormInterface $imagesForm)
    {
        foreach ($imagesForm as $imageForm) {
            $imageFile = $imageForm['imageInput']->getData();
            if (empty($imageFile)) {
                continue;
            }
            $trickImage = $imageForm->getData();
            $trickImage->setFileName($this->uploader->upload($imageForm['imageInput']->getData()));
        }
    }

    private function handleVideosSubmission($videos) {
        foreach ($videos as $video) {
            if (empty($video->getUrl())) {
                continue;
            }
            $embed = new Embed();
            $info = $embed->get($video->getUrl());
            $video->setEmbedCode($info->code->html);
        }
    }

    public function handleFormSubmission(FormInterface $form, Trick $trick): void
    {
        $this->trick = $trick;
        $trick->setSlug($this->slugger->slug($trick->getTitle()));

        $mainImage = $form['mainImage'];
        $imagesForm = $form['images'];
        $videos = $trick->getVideos();

        if ($mainImage) {
            $image = $this->handleMainImageSubmission($mainImage);
            $this->trick->setMainImage($image);
        }

        if ($imagesForm) {
            $this->handleImagesSubmission($imagesForm);
        }

        if ($videos) {
            $this->handleVideosSubmission($videos);
        }
        $this->entityManager->persist($trick);
        $this->entityManager->flush();
    }
}