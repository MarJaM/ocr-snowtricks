<?php


namespace App\Service;


use App\Entity\Token;
use App\Entity\User;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EmailSender
{
    /**
     * @var MailerInterface $mailer
     */
    private $mailer;

    /**
     * @var UrlGeneratorInterface $mailer
     */
    private $router;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $router)
    {
        $this->mailer = $mailer;
        $this->router = $router;
    }

    /**
     * @param User $user
     * @throws TransportExceptionInterface
     */
    public function sendEmailVerificationEmail(User $user): void
    {
        $message = 'Hi ' . $user->getPseudo() . ',' . PHP_EOL .
            'Welcome on Snowtricks. Before enjoying the snow, you need to confirm your email address by clicking on this link:' . PHP_EOL . PHP_EOL .
            $this->router->generate('verify_email', [
                'token' => $user->getTokens()->last()->getToken(),
                'email' => $user->getEmail(),
            ],UrlGeneratorInterface::ABSOLUTE_URL);

        $this->sendEmail('Confirm your email!', $message, $user->getEmail());
    }

    /**
     * @param User $user
     * @throws TransportExceptionInterface
     */
    public function sendResetPasswordEmail(User $user): void
    {
        $message = 'Hi ' . $user->getPseudo() . ',' . PHP_EOL .
            'Please use the following link to reset your password:' . PHP_EOL . PHP_EOL .
            $this->router->generate('reset_password', [
                'token' => $user->getTokens()->last()->getToken(),
                'email' => $user->getEmail(),
            ],UrlGeneratorInterface::ABSOLUTE_URL);

        $this->sendEmail('Reset your password', $message, $user->getEmail());
    }

    /**
     * @param string $subject
     * @param string $message
     * @param string $recipientAddress
     * @throws TransportExceptionInterface
     */
    private function sendEmail(string $subject, string $message, string $recipientAddress): void
    {
        $email = (new Email())
            ->from(Address::fromString('Snowtricks <mj@mariusjammes.fr>'))
            ->to($recipientAddress)
            ->subject($subject)
            ->text($message);

        $this->mailer->send($email);
    }
}