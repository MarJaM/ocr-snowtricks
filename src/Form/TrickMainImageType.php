<?php


namespace App\Form;


use App\Entity\TrickMainImage;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrickMainImageType extends TrickImageType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => TrickMainImage::class,
                               ]);
    }
}