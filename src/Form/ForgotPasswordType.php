<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ForgotPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $inputClass = ['class' => 'input'];
        $builder
            ->add('pseudo', TextType::class, [
                'attr' => $inputClass,
                'label' => 'Username',
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'button'],
                'label' => 'Ask for reset password'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['forgot_password'],
        ]);
    }
}
