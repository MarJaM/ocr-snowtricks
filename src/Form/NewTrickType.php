<?php


namespace App\Form;


use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class NewTrickType extends AbstractTrickType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add(
            'mainImage',
            TrickMainImageType::class,
            [
                'required' => false,
            ]
        )
            ->add(
                'images',
                CollectionType::class,
                [
                    'entry_type' => TrickImageType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => false,
                    'by_reference' => false,
                ]
            )
            ->add(
                'videos',
                CollectionType::class,
                [
                    'entry_type' => TrickVideoType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => false,
                    'by_reference' => false,
                ]
            );
    }
}