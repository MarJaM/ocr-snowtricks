<?php


namespace App\Form;


use App\Entity\TrickImage;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class TrickImageType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                'imageInput',
                FileType::class,
                [
                    'constraints' => [new Image()],
                    'label' => false,
                    'attr' => [
                        'class' => 'file-input',
                        'accept' => 'image/*'
                    ],
                    'mapped' => false,
                ]
            )
            ->add(
                'fileName',
                HiddenType::class
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => TrickImage::class,
                               ]);
    }
}