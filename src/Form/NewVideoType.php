<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'url',
                UrlType::class,
                [
                    'attr' => [
                        'type' => 'url',
                        'class' => 'input',
                        'placeholder' => "Link to the video"
                    ],
                    'default_protocol' => 'https',
                ]
            );
    }
}
