<?php

namespace App\Repository;

use App\Entity\Trick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trick|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trick|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trick[]    findAll()
 * @method Trick[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrickRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trick::class);
    }

    /**
     * @param int $page
     * @return Paginator
     */

    public function findPage(int $page): Paginator
    {
        $query = $this->createQueryBuilder('t')
            ->orderBy('t.id', 'ASC')
            ->setFirstResult(10 * ($page - 1))
            ->setMaxResults(10 * $page)
            ->getQuery()
        ;

        return new Paginator($query);
    }
}
