<?php

namespace App\Repository;

use App\Entity\TrickMainImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrickMainImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrickMainImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrickMainImage[]    findAll()
 * @method TrickMainImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrickMainImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrickMainImage::class);
    }

    // /**
    //  * @return TrickMainImage[] Returns an array of TrickMainImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TrickMainImage
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
