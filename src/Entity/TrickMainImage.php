<?php

namespace App\Entity;

use App\Repository\TrickMainImageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrickMainImageRepository::class)
 */
class TrickMainImage extends MediaImage
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Trick::class, inversedBy="mainImage", cascade={"persist", "remove"})
     */
    private $trickOwner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrickOwner(): ?Trick
    {
        return $this->trickOwner;
    }

    public function setTrickOwner(?Trick $trickOwner): self
    {
        $this->trickOwner = $trickOwner;

        return $this;
    }
}
