# Snowtricks

## Presentation

This is a project made for an Openclassrooms project. The objective was to build a snowboard website with the ability to add/remove/update/delete snowboard tricks.

A live version of the site is available at https://snowtricks.mariusjammes.fr/

Quality of code has been analyse by [Codacy](http://app.codacy.com/manual/mj/ocr-snowtricks/dashboard?bid=18098174&dashboardType=Days31&token=5NSA9gA5LFeoBs5).

## Requirements

To be able to test the project in a local environment, you will need :

* PHP 7.4+
* composer
* npm
* MySQL / MariaDB
* [Symfony-CLI](https://symfony.com/download)

## Installation on a local environment

1. Clone the project : `git clone git@gitlab.com:MarJaM/ocr-snowtricks.git`
2. Go into the project repository : `cd ocr-snowtricks`
3. Install dependencies : `composer install && npm install`
4. Create you .env.local file : `touch .env.local`
5. Add your own [DATABASE_URL](https://symfony.com/doc/current/doctrine.html#configuring-the-database) and [MAILER_DSN](https://symfony.com/doc/current/mailer.html#installation) environment variables into the .env.local file.
6. Setup the dabatase : `php bin/console doctrine:database:create`
7. You have two possibilities here : 
    1. Use the init.dump file provided into the fixtures folder to initiate a set of data `php bin/console doctrine:database:import fixtures/init.dump`. You will also need to copy/paste the content of fixtures/htdocs/uploads folder into the htdocs/uploads folder.
    2. Initiate the database schema without any data : `php bin/console doctrine:schema:create`
8. Run webpack to build css and javascript assets : `npm run watch`
9. Run symfony local server : `symfony server:start --dir=htdocs`
10. Open your browser, go to http://localhost:8000 and enjoy!

If you used the fixtures provided, credentials are :

* admin@snowtricks.org / admin
* user@snowtricks.org / user